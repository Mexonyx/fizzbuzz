import script
import pytest


def testIsNotMultiple():
    assert script.fizzBuzz(8) == "8"

def testIsMultipleThree():
    assert script.fizzBuzz(33) == "Fizz"

def testIsMultipleFive():
    assert script.fizzBuzz(20) == "Buzz"

def testIsMultipleFiveThree():
    assert script.fizzBuzz(30) == "FizzBuzz"