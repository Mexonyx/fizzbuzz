
# Implémentez une fonction fizzBuzz prenant en entrée un nombre entier et retournant ce même nombre sous
# forme de chaine de caractères.
# Quelques exceptions sont à prendre en compte :
# • pour les multiples de 3, la fonction retourne "Fizz"
# • pour les multiples de 5, la fonction retourne "Buzz"
# • pour les multiples de 3 et de 5, la fonction retourne "FizzBuzz"


def fizzBuzz(nombre):
    result = ''
    if nombre % 5 == 0 and nombre % 3 == 0:
        result = "FizzBuzz"
    elif nombre % 3 == 0:
        result = "Fizz"
    elif nombre % 5 == 0:
        result = "Buzz"
    else:
        result = str(nombre)

    return result





if __name__ == "__main__":
    insertNumber = True
    while insertNumber:
        inputUser = input("Veuillez tapper le nombre que vous souhaitez (mettez 'stop' pour arrêter): ")
        if(inputUser.lower() == "stop"):
            insertNumber = False
        else:
            print(fizzBuzz(int(inputUser)))
